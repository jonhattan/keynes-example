import hudson.model.*
import hudson.plugins.filesystem_scm.*
import hudson.plugins.git.extensions.GitSCMExtension
import org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition
import org.jenkinsci.plugins.workflow.job.WorkflowJob
import jenkins.model.Jenkins

def env = System.getenv()
String keynesJobName=env.KEYNES_SEED_LOCAL_JOB_NAME
String keynesDirectory=env.KEYNES_SEED_LOCAL_DIRECTORY

if (!Jenkins.instance.getJobNames().contains(keynesJobName)) {
  def scm = new FSSCM(keynesDirectory, false, false, new FilterSettings(true, Collections.<FilterSelector>emptyList()))

  WorkflowJob job = new WorkflowJob(Jenkins.instance, keynesJobName)
  job.setDescription('Bootstraps the Jenkins server by installing all jobs (using the jobDSL plugin)')
  job.setDefinition(new CpsScmFlowDefinition(scm, 'docker/Jenkinsfile'))
  job.save()

  Jenkins.instance.restart()
}

